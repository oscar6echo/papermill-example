
# Papermill Example

## 1 - Overview

This repo is a simple example of how to use [papermill](https://papermill.readthedocs.io/en/latest/index.html) to run batches from workflow notebooks.  

papermill was created by and is used in production at Netflix (See their article [Beyond Interactive and notebooks innovation at Netflix](https://medium.com/netflix-techblog/notebook-innovation-591ee3221233)).

Thus you can keep the interactive experience of a notebook, but also run it as part of a batch process, setting variables from outside (using tags) and saving a copy of the run notebook.


## 2 - Run

From terminal, in an environment containing `notebook`, `pandas` and `papermill`:

```bash
$ git clone https://gitlab.com/oscar6echo/papermill-example.git
cd papermill-example

# to run workflow notebooks interactively - typically long running tasks
$ jupyter notebook
# open workflow-A.ipynb, workflow-B.ipynb
# the workflow notebooks output data in folder output-data/

# to run the notebooks as batch
$ python batch.py
# a copy of the notebooks post run are saved in folder output-notebooks/
# available for analysis if workflow did not go as expected

# to view the results saved in output-data/
# open check.ipynb
```

## 3 - Files

For a quick view of the files:
+ [workflow-A.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/oscar6echo/papermill-example/raw/master/workflow-A.ipynb)
+ [workflow-B.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/oscar6echo/papermill-example/raw/master/workflow-B.ipynb)
+ [batch.py](https://gitlab.com/oscar6echo/papermill-example/blob/master/batch.py)
+ [check.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/oscar6echo/papermill-example/raw/master/check.ipynb)



