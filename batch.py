
import os
import papermill as pm

folder = 'output-notebooks'
if not os.path.exists(folder):
    os.makedirs(folder)

pm.execute_notebook(
    'workflow-A.ipynb',
    os.path.join(folder, 'workflow-A-run.ipynb'),
    parameters={
        'a': 123456
    }
)

pm.execute_notebook(
    'workflow-B.ipynb',
    os.path.join(folder, 'workflow-B-run.ipynb'),
    parameters={
        'a': 2222222
    }
)
