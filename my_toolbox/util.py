
import os
import pickle
import gzip

from timeit import default_timer as timer


class Util:
    """
    """

    @staticmethod
    def topickle(path,
                 obj,
                 compresslevel=9):
        """
        pickle obj to disk
        compresslevel from 0 to 9
        9 is the slowest, most compressed
        """
        pickle.dump(obj=obj,
                    file=gzip.open(path,
                                   'wb',
                                   compresslevel=compresslevel),
                    protocol=pickle.HIGHEST_PROTOCOL)

    @staticmethod
    def unpickle(path):
        """
        unpickle obj from disk
        """
        return pickle.load(gzip.open(path, 'rb'))

    @staticmethod
    def save_csv(name,
                 df,
                 folder='dump',
                 verbose=True):
        """
        save df to disk
        """
        if not os.path.exists(folder):
            os.makedirs(folder)

        t0 = timer()
        filename = name+'.csv'
        path = os.path.join(folder, filename)
        df.to_csv(path)
        t1 = timer()
        if verbose:
            print('file {} saved in {:.2f} s'.format(path, t1-t0))

    @staticmethod
    def save_pk(name,
                obj,
                folder='dump',
                compresslevel=9,
                verbose=True):
        """
        save df to disk
        """
        if not os.path.exists(folder):
            os.makedirs(folder)
        t0 = timer()
        filename = name+'.pk'
        path = os.path.join(folder, filename)
        Util.topickle(path, obj, compresslevel=compresslevel)
        t1 = timer()
        if verbose:
            print('file {} saved in {:.2f} s'.format(path, t1-t0))
